<?php

declare(strict_types=1);

use App\Http\EndpointEnum;
use App\Http\Router;
use App\Post\FetchPostRequest;
use App\Statistics\StatisticController;
use App\Statistics\QuestionFactory;
use App\Token\TokenRegister;
use Symfony\Component\HttpClient\HttpClient;

require dirname(__FILE__).'/vendor/autoload.php';

$noCache = filter_input(INPUT_GET, 'nocache', FILTER_VALIDATE_BOOLEAN) ?? false;

$memcached = new Memcached();
$memcached->addServer("memcached", 11211);

if ($noCache || !$memcached->get('posts')) {
    $httpClient = HttpClient::create();

    $tokenRegister = new TokenRegister($httpClient);
    $token = $tokenRegister->getNewTokenFromAPI();

    $fetchPostRequest = new FetchPostRequest($token, $httpClient);
    $postsCollection = $fetchPostRequest->fetchAllPosts();

    $memcached->set('posts', $postsCollection);
}

$factory = new QuestionFactory($memcached->get('posts'));

$controller = new StatisticController($factory);

//Endpoints declaration
Router::route(EndpointEnum::AVERAGE, function () use ($controller) {
    return $controller->average();
});

Router::route(EndpointEnum::LONGEST, function () use ($controller) {
    return $controller->longest();
});

Router::route(EndpointEnum::TOTAL_BY_WEEK, function () use ($controller) {
    return $controller->total();
});

Router::route(EndpointEnum::AVERAGE_BY_USER_MONTHLY, function () use ($controller) {
    return $controller->averageByUser();
});

$action = $_SERVER['REQUEST_URI'];
Router::dispatch($action);
