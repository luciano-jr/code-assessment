<?php

declare(strict_types=1);

namespace App\Statistics;

final class Answer
{
    /**
     * Answer constructor.
     * @param array $output
     */
    public function __construct(private array $output)
    {}

    /**
     * @return array
     */
    public function output(): array
    {
        return $this->output;
    }
}
