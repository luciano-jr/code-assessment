<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Post\Post;
use App\Statistics\Answer;

final class LongestPostByCharacterLengthPerMonth extends AbstractQuestion
{
    /**
     * @return Answer
     */
    public function answer(): Answer
    {
        $output = [];
        $longestPost = [];
        foreach ($this->collection as $postItem) {
            $post = Post::fromArray($postItem);

            $month = $post->month();
            $length = $post->messageLength();

            $compareTo = $output[$month] ?? 0;

            if ($length > $compareTo) {
                $output[$month] = $length;
                $longestPost[$month] = $post;
            }
        }

        return new Answer($longestPost);
    }
}
