<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

interface QuestionTypeInterface
{
    public const AVERAGE_CHARACTER_LENGTH_OF_POSTS_PER_MONTH = AverageCharacterLengthOfPostsPerMonth::class;
    public const LONGEST_POST_BY_CHARACTER_LENGTH_PER_MONTH = LongestPostByCharacterLengthPerMonth::class;
    public const TOTAL_POSTS_SPLIT_BY_WEEK_NUMBER = TotalPostsSplitByWeekNumber::class;
    public const AVERAGE_NUMBER_OF_POSTS_PER_USER_PER_MONTH = AverageNumberOfPostsPerUserPerMonth::class;
}
