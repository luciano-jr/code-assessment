<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Post\Post;
use App\Statistics\Answer;

final class AverageNumberOfPostsPerUserPerMonth extends AbstractQuestion
{
    /**
     * @return Answer
     */
    public function answer(): Answer
    {
        $output = [];
        foreach ($this->collection as $postItem) {
            $post = Post::fromArray($postItem);

            $user = $post->user();
            $month = $post->month();

            if (!isset($output[$user][$month])) {
                $output[$user][$month] = 0;
            }

            $output[$user][$month] ++;
        }

        return new Answer($output);
    }
}
