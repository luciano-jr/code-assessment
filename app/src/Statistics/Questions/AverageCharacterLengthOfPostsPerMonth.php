<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Post\Post;
use App\Statistics\Answer;

final class AverageCharacterLengthOfPostsPerMonth extends AbstractQuestion
{
    /**
     * @return Answer
     */
    public function answer(): Answer
    {
        $output = [];
        foreach ($this->collection as $postItem) {
            $post = Post::fromArray($postItem);
            $words = explode(" ", $post->message());
            $wordsLength = array_map('strlen', $words);
            $month = $post->month();

            $output[$month][] = $this->average($wordsLength);
        }

        foreach ($output as $month => $averages) {
            $output[$month] = $this->average($averages);
        }

        return new Answer($output);
    }

    /**
     * @param $array
     * @return float
     */
    private function average($array): float
    {
        $carry = null;
        $count = count($array);
        return array_reduce($array, function ($carried, $value) use ($count) {
            return ($carried === null ? 0 : $carried) + ($value / $count);
        }, $carry);
    }
}
