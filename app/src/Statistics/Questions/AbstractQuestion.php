<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Post\PostCollection;

abstract class AbstractQuestion implements QuestionInterface
{
    /**
     * AbstractQuestion constructor.
     * @param PostCollection $collection
     */
    public function __construct(protected PostCollection $collection) {}
}
