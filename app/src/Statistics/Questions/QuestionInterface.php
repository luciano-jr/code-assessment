<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Statistics\Answer;

interface QuestionInterface
{
    public function answer(): Answer;
}
