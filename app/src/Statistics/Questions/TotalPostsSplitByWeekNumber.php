<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Post\Post;
use App\Statistics\Answer;

final class TotalPostsSplitByWeekNumber extends AbstractQuestion
{
    /**
     * @return Answer
     */
    public function answer(): Answer
    {
        $output = [];
        foreach ($this->collection as $postItem) {
            $post = Post::fromArray($postItem);

            $week = $post->weekNumber();

            if (!isset($output[$week])) {
                $output[$week] = 0;
            }

            $output[$week] ++;
        }

        return new Answer($output);
    }
}
