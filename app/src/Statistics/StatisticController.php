<?php

declare(strict_types=1);

namespace App\Statistics;

use App\Statistics\Questions\QuestionTypeInterface;

final class StatisticController
{
    /**
     * StatisticController constructor.
     * @param QuestionFactory $factory
     */
    public function __construct(private QuestionFactory $factory)
    {}

    /**
     * @return array
     * @throws \Exception
     */
    public function average(): array
    {
        return $this->factory
            ->create(QuestionTypeInterface::AVERAGE_CHARACTER_LENGTH_OF_POSTS_PER_MONTH)
            ->answer()
            ->output();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function longest(): array
    {
        return $this->factory
            ->create(QuestionTypeInterface::LONGEST_POST_BY_CHARACTER_LENGTH_PER_MONTH)
            ->answer()
            ->output();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function averageByUser(): array
    {
        return $this->factory
            ->create(QuestionTypeInterface::AVERAGE_NUMBER_OF_POSTS_PER_USER_PER_MONTH)
            ->answer()
            ->output();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function total(): array
    {
        return $this->factory
            ->create(QuestionTypeInterface::TOTAL_POSTS_SPLIT_BY_WEEK_NUMBER)
            ->answer()
            ->output();
    }
}
