<?php

declare(strict_types=1);

namespace App\Statistics;

use App\Post\PostCollection;
use App\Statistics\Questions\AverageCharacterLengthOfPostsPerMonth;
use App\Statistics\Questions\AverageNumberOfPostsPerUserPerMonth;
use App\Statistics\Questions\LongestPostByCharacterLengthPerMonth;
use App\Statistics\Questions\QuestionInterface;
use App\Statistics\Questions\QuestionTypeInterface;
use App\Statistics\Questions\TotalPostsSplitByWeekNumber;

final class QuestionFactory
{
    /**
     * QuestionFactory constructor.
     * @param PostCollection $collection
     */
    public function __construct(private PostCollection $collection)
    {}

    /**
     * @param string $type
     * @return QuestionInterface
     * @throws \Exception
     */
    public function create(string $type): QuestionInterface
    {
        return match ($type) {
            QuestionTypeInterface::AVERAGE_CHARACTER_LENGTH_OF_POSTS_PER_MONTH => new AverageCharacterLengthOfPostsPerMonth($this->collection),
            QuestionTypeInterface::LONGEST_POST_BY_CHARACTER_LENGTH_PER_MONTH => new LongestPostByCharacterLengthPerMonth($this->collection),
            QuestionTypeInterface::TOTAL_POSTS_SPLIT_BY_WEEK_NUMBER => new TotalPostsSplitByWeekNumber($this->collection),
            QuestionTypeInterface::AVERAGE_NUMBER_OF_POSTS_PER_USER_PER_MONTH => new AverageNumberOfPostsPerUserPerMonth($this->collection),
            default => throw new \Exception(sprintf('There is no such type of question: %s', $type)),
        };
    }
}
