<?php

declare(strict_types=1);

namespace App\Http;

final class EndpointEnum
{
    public const AVERAGE = '/stats/average';
    public const LONGEST = '/stats/longest';
    public const TOTAL_BY_WEEK = '/stats/total';
    public const AVERAGE_BY_USER_MONTHLY = '/stats/average-by-user';

    public static array $endpoints = [
        self::AVERAGE,
        self::LONGEST,
        self::TOTAL_BY_WEEK,
        self::AVERAGE_BY_USER_MONTHLY
    ];
}
