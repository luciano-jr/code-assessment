<?php

declare(strict_types=1);

namespace App\Http;

use Closure;
use Exception;

final class Router
{
    private static array $routes = [];

    /**
     * Router constructor.
     * @param array $routes
     */
    public function __construct(array $routes = [])
    {
        self::$routes = $routes;
    }

    /**
     * @param string $action
     * @param Closure $callback
     */
    public static function route(string $action, Closure $callback): void
    {
        $action = trim($action, '/');
        self::$routes[$action] = $callback;
    }

    /**
     * @param string $action
     * @throws Exception
     */
    public static function dispatch(string $action): void
    {
        $parsedUrl = parse_url($action);
        $action = trim($parsedUrl['path'], '/');

        if (empty($action) || !isset(self::$routes[$action])) {
            echo sprintf('%u: try these links instead: <br /><br /> ', 404);

            foreach (EndpointEnum::$endpoints as $endpoint) {
                echo sprintf('http://%s%s <br />', 'localhost:8000', $endpoint);
            }
            return;
        }

        $callback = self::$routes[$action];

        if (empty($callback)) {
            throw new Exception(sprintf('Endpoint not found: %s', $callback));
        }

        echo json_encode(call_user_func($callback), JSON_OBJECT_AS_ARRAY);
    }
}
