<?php

declare(strict_types=1);

namespace App\Post;

use App\Token\Token;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class FetchPostRequest
{
    public const ENDPOINT = 'posts';

    public const PAGE_PARAMETER = 'page';

    /**
     * FetchPostRequest constructor.
     * @param Token $token
     * @param HttpClientInterface $client
     */
    public function __construct(
        public Token $token,
        public HttpClientInterface $client
    ) {}

    /**
     * @param int $page
     * @return PostCollection
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function fetchAllPosts(int $page = 1): PostCollection
    {
        $postCollection = new PostCollection();

        do {
            $responseData = $this->fetchPosts($page);
            $postCollection->addPosts($responseData['data']);
            $page ++;
        } while ($page <= 10);

        return $postCollection;
    }

    /**
     * @param int $page
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function fetchPosts(int $page = 1): array
    {
        $response = $this->client->request('GET', 'https://api.supermetrics.com/assignment/posts', [
            'query' => [
                $this->token::PARAMETER_NAME => $this->token->value(),
                self::PAGE_PARAMETER => $page
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        return $response->toArray();
    }
}
