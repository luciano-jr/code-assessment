<?php

declare(strict_types=1);

namespace App\Post;

final class PostCollection implements \Iterator, \Countable
{
    private array $array = [];

    private int $position = 0;

    /**
     * PostCollection constructor.
     */
    public function __construct()
    {
        $this->position = 0;
    }

    /**
     * @param array $posts
     */
    public function addPosts(array $posts): void
    {
        array_push($this->array, ...$posts['posts']);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * @return mixed
     */
    public function current(): mixed
    {
        return $this->array[$this->position];
    }

    /**
     * @return int
     */
    public function key(): int
    {
        return $this->position;
    }

    public function next(): void
    {
        ++$this->position;
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->array[$this->position]);
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->array);
    }
}
