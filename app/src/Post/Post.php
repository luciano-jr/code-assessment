<?php

declare(strict_types=1);

namespace App\Post;

final class Post
{
    /**
     * Post constructor.
     * @param string $id
     * @param string $message
     * @param string $type
     * @param string $createdTime
     * @param string $author
     */
    public function __construct(
        public string $id,
        public string $message,
        public string $type,
        public string $createdTime,
        public string $author
    ) {}

    public static function fromArray(array $content): self
    {
        return new Post(
            $content['id'],
            $content['message'],
            $content['type'],
            $content['created_time'],
            $content['from_id']
        );
    }

    public function month(): string
    {
        return date('Y-m', strtotime($this->createdTime));
    }

    public function messageLength(): int
    {
        return strlen($this->message);
    }

    public function weekNumber(): int
    {
        return (int) date('W', strtotime($this->createdTime));
    }

    public function user(): string
    {
        return $this->author;
    }

    public function message(): string
    {
        return $this->message;
    }
}
