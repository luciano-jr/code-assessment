<?php

declare(strict_types=1);

namespace App\Token;

final class Token
{
    public const PARAMETER_NAME = 'sl_token';

    /**
     * Token constructor.
     * @param string $value
     */
    public function __construct(private string $value)
    {}

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }
}
