<?php

declare(strict_types=1);

namespace App\Token;

use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class TokenRegister
{
    private const CLIENT_ID = 'client_id';
    private const EMAIL = 'email';
    private const NAME = 'name';

    /**
     * TokenRegister constructor.
     * @param HttpClientInterface $client
     */
    public function __construct(private HttpClientInterface $client)
    {}

    /**
     * @return Token
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function getNewTokenFromAPI(): Token
    {
        try {
            $response = $this->client->request('POST', 'https://api.supermetrics.com/assignment/register', [
                'body' => [
                    self::CLIENT_ID => 'ju16a6m81mhid5ue1z3v2g0uh',
                    self::EMAIL => 'luciano@lucianojr.com.br',
                    self::NAME => 'Luciano Jr'
                ]
            ]);

            if ($response->getStatusCode() !== 200) {
                throw new Exception(sprintf('API error: %s', $response->getBody()->getContents()));
            }

            $responseData = $response->toArray();

            return new Token($responseData['data']['sl_token']);

        } catch (TransportExceptionInterface $exception) {
            throw new Exception(sprintf('API exception: %s', $exception->getMessage()));
        }
    }
}
