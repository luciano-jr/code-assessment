<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Post\PostCollection;
use PHPUnit\Framework\TestCase;

final class AverageNumberOfPostsPerUserPerMonthTest extends TestCase
{
    public function testAnswerGivenThreeAuthorsWithOnePostEachShouldSumOne(): void
    {
        $collection = new PostCollection();
        $posts = [
            ['id' => 'caa262fd-07c8-4fcd-b1ce-5701966418e9', 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foo'],
            ['id' => 'f28732f1-ba9e-4a8b-96b2-b2e669362ab1', 'from_id' => 'McTommy', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foobaz'],
            ['id' => 'be26dbec-df6a-4971-b708-f06a16da7b87', 'from_id' => 'Ada', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Bar']
        ];
        $collection->addPosts(['posts' => $posts]);

        $question = new AverageNumberOfPostsPerUserPerMonth($collection);
        $answer = $question->answer();
        $output = $answer->output();

        $this->assertCount(3, $output);
        $this->assertCount(1, $output['John Doe']);
        $this->assertCount(1, $output['McTommy']);
        $this->assertCount(1, $output['Ada']);
    }

    public function testAnswerGivenTwoPostsInDifferentMonthsForSameAuthor(): void
    {
        $collection = new PostCollection();
        $posts = [
            ['id' => 'caa262fd-07c8-4fcd-b1ce-5701966418e9', 'from_id' => 'McTommy', 'type' => 'status', 'created_time' => '2021-02-05T12:00:35+00:00', 'message' => 'Foo'],
            ['id' => 'f28732f1-ba9e-4a8b-96b2-b2e669362ab1', 'from_id' => 'McTommy', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foobaz'],
        ];
        $collection->addPosts(['posts' => $posts]);

        $question = new AverageNumberOfPostsPerUserPerMonth($collection);
        $answer = $question->answer();
        $output = $answer->output();

        $this->assertCount(1, $output);
        $this->assertCount(2, $output['McTommy']);
    }
}
