<?php

declare(strict_types=1);

namespace App\Tests\Statistics\Questions;

use App\Post\PostCollection;
use App\Statistics\Questions\AverageCharacterLengthOfPostsPerMonth;
use PHPUnit\Framework\TestCase;

final class AverageCharacterLengthOfPostsPerMonthTest extends TestCase
{
    public function testAnswerGivenOnePostShouldReturnSameLength(): void
    {
        $collection = new PostCollection();
        $collection->addPosts([
            'posts' => [['id' => '08fd3db6-0e11-4fea-8997-d268287d88fe', 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foo']]
        ]);
        $handler = new AverageCharacterLengthOfPostsPerMonth($collection);

        $answer = $handler->answer();
        $output = $answer->output();
        $this->assertIsArray($output);
        $this->assertEquals(3, $output['2021-01']);
    }

    public function testAnswerGivenThreePostsShouldReturnTheAverageOfThemAsResult(): void
    {
        $collection = new PostCollection();
        $collection->addPosts([
            'posts' => [
                ['id' => 'bf772bbb-17f8-426b-b77e-650f04dec475', 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foo'],
                ['id' => 'c8753253-6f39-4ca1-aa67-3e24ea1eefcf', 'from_id' => 'McTommy', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foobaz'],
                ['id' => '12be8502-9148-412c-9fd5-4110ddd0c24f', 'from_id' => 'Ada', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Bar']
            ]
        ]);
        $handler = new AverageCharacterLengthOfPostsPerMonth($collection);

        $answer = $handler->answer();
        $output = $answer->output();
        $this->assertEquals(4, $output['2021-01']);
    }
}
