<?php

declare(strict_types=1);

namespace App\Statistics\Questions;

use App\Post\Post;
use App\Post\PostCollection;
use PHPUnit\Framework\TestCase;

final class LongestPostByCharacterLengthPerMonthTest extends TestCase
{
    public function testAnswerGivenPostWith5LettersShouldReturnItsPost(): void
    {
        $collection = new PostCollection();
        $posts = [
            ['id' => 'caa262fd-07c8-4fcd-b1ce-5701966418e9', 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foo'],
            ['id' => 'f28732f1-ba9e-4a8b-96b2-b2e669362ab1', 'from_id' => 'McTommy', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foobaz'],
            ['id' => 'be26dbec-df6a-4971-b708-f06a16da7b87', 'from_id' => 'Ada', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Bar']
        ];
        $collection->addPosts(['posts' => $posts]);

        $longestPost = Post::fromArray($posts[1]);
        $question = new LongestPostByCharacterLengthPerMonth($collection);
        $answer = $question->answer();
        $output = $answer->output();

        $this->assertCount(1, $output);
        $this->assertEquals($longestPost, $output['2021-01']);
    }

    public function testAnswerGivenTwoPostWithDifferentWeekNumberShouldReturnEachPostInTheWeekNumber(): void
    {
        $collection = new PostCollection();
        $posts = [
            ['id' => 'caa262fd-07c8-4fcd-b1ce-5701966418e9', 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-01-15T12:00:35+00:00', 'message' => 'Foo'],
            ['id' => 'f28732f1-ba9e-4a8b-96b2-b2e669362ab1', 'from_id' => 'McTommy', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foobaz'],
            ['id' => 'be26dbec-df6a-4971-b708-f06a16da7b87', 'from_id' => 'Ada', 'type' => 'status', 'created_time' => '2021-02-05T12:00:35+00:00', 'message' => 'Foobar']
        ];
        $collection->addPosts(['posts' => $posts]);

        $longestPostMonthOne = Post::fromArray($posts[1]);
        $longestPostMonthTwo = Post::fromArray($posts[2]);
        $question = new LongestPostByCharacterLengthPerMonth($collection);
        $answer = $question->answer();
        $output = $answer->output();

        $this->assertCount(2, $output);
        $this->assertEquals($longestPostMonthOne, $output['2021-01']);
        $this->assertEquals($longestPostMonthTwo, $output['2021-02']);
    }

}
