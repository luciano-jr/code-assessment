<?php

declare(strict_types=1);

namespace App\Tests\Statistics\Questions;

use App\Post\Post;
use App\Post\PostCollection;
use App\Statistics\Questions\TotalPostsSplitByWeekNumber;
use PHPUnit\Framework\TestCase;

class TotalPostsSplitByWeekNumberTest extends TestCase
{
    public function testAnswerGivenOnePostShouldReturnItsWeekNumberAndOneAsResult(): void
    {
        $collection = new PostCollection();
        $posts = ['id' => 1, 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foo'];
        $collection->addPosts(['posts' => [$posts]]);
        $post = Post::fromArray($posts);

        $question = new TotalPostsSplitByWeekNumber($collection);

        $answer = $question->answer();
        $output = $answer->output();
        $this->assertIsArray($output);
        $this->assertArrayHasKey($post->weekNumber(), $output);
        $this->assertCount(1, $output);
        $this->assertEquals(1, $output[$post->weekNumber()]);
    }

    public function testAnswerGivenTwoPostsShouldReturnItsWeekNumbersAndPostCountAsResult(): void
    {
        $collection = new PostCollection();
        $postItemOne = ['id' => 1, 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-01-05T12:00:35+00:00', 'message' => 'Foo'];
        $postItemTwo = ['id' => 2, 'from_id' => 'John Doe', 'type' => 'status', 'created_time' => '2021-07-05T12:00:35+00:00', 'message' => 'Bar'];
        $collection->addPosts(['posts' => [$postItemOne, $postItemTwo]]);

        $question = new TotalPostsSplitByWeekNumber($collection);
        $answer = $question->answer();
        $output = $answer->output();

        $postOne = Post::fromArray($postItemOne);
        $postTwo = Post::fromArray($postItemTwo);

        $this->assertIsArray($output);
        $this->assertArrayHasKey($postOne->weekNumber(), $output);
        $this->assertArrayHasKey($postTwo->weekNumber(), $output);
        $this->assertCount(2, $output);
        $this->assertEquals(1, $output[$postOne->weekNumber()]);
        $this->assertEquals(1, $output[$postTwo->weekNumber()]);
    }
}
