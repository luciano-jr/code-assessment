<?php

declare(strict_types=1);

namespace App\Tests\Token;

use App\Token\TokenRegister;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;

class TokenRegisterTest extends TestCase
{
    public function testGetNewToken(): void
    {
        $httpClient = HttpClient::create();

        $tokenRegister = new TokenRegister($httpClient);
        $token = $tokenRegister->getNewTokenFromAPI();

        $this->assertIsString($token->value());
        $this->assertNotEmpty($token->value());
    }
}
