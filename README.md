# Code assessment

## Installation

Use [docker](https://docs.docker.com/install) to install the container services.

```bash
docker-compose up -d
```

This command will install an instance of nginx, php-fpm and memcached service.


## Usage

You can get the postman collection using the button:


[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/e834cef54b7e9b0a40c4)

There are few endpoints in the collection:

`/stats/average` Average character length of posts per month

`/stats/longest` The longest post by character length per month

`/stats/total` Total posts split by week number

`/stats/average-by-user` Average number of posts per user per month
