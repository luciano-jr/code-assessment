FROM php:8.0.0-fpm

RUN apt-get update && apt-get install -y git libmemcached-dev zlib1g-dev \
    && pecl install memcached \
    && docker-php-ext-enable memcached

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /var/www
